<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<html lang="en">

<head>
	<title>Trade Dangerous</title>
	<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
	<meta http-equiv="content-language" content="en">
	<meta name="description" content="Trade Dangerous - Trade Program">
	<meta name="author" content="Web Design - Paul A. Rocco  Trade Dangerou - Oliver Smith  Elite Dangerous - Frontier Developments plc">
	<meta name="version" content="1.0">
	<meta name="copyright" content="N/A">
	<meta name="viewport" content="width=device-width,user-scalable=0,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0">
	<link rel="shortcut icon" href="/favicon.ico?v=2">
	<style type="text/css">
	</style>
	<div class="auto-style6">
	<img height="512" src="/TD.png" width="640"><style type="text/css">
</head>
<form name="form" method="post" action="tester.php">
<style type="text/css">
.auto-style1 {
	text-align: center;
	background-color: #000000;
}
.auto-style3 {
	text-align: center;
	color: #7FFF00;
}
.auto-style4 {
	text-decoration: underline;
}
.auto-style5 {
	text-align: center;
}
.auto-style6 {
   text-align: center;
	margin-top: 0px;
}
.auto-style7 {
   text-align: center;
	margin-top: 0px;
	color: #FFFFFF;
	font-size: x-small;
}
</style>


<body bgcolor="#000000" style="height: 492px; margin-top: 0px" background="/Wiki-background.png">
<form name="form" method="post" action="tester.php" background="/Wiki-background.png" color= "#7FFF00";>
	<div class="auto-style3">
		<div class="auto-style1">
			Welcome Commander!
     </td>
  </tr>

 <td>
         </td>
  <br>
  <tr>
     <td>
        <input  type="text" name="command_line" maxlength="500" size="50"></td></div>
		<p class="auto-style1">
        <input type="submit" value="Submit"></p>
		<div class="auto-style5">
		<span class="auto-style1">DO NOT RUN "trade.py" IN THE COMMAND LINE.
			<br></span>RUN SEARCHES LIKE "<strong><em>-v run --ship hauler --cr 
			50000 --from chango</em></strong>"<span class="auto-style4"><br><br>ALL COMMANDS BEFORE USING "RUN"</span><br>
			<br>&nbsp;usage: [-h] [--debug] [--detail] [--quiet] 
						<br><br>Common Switches:<br><strong>-h, --help 
					</strong>Show this help message and exit. <br><strong>--debug, -w</strong> 
					Enable diagnostic output. <br><strong>--detail, -v</strong> 
					Increase level of detail in output. <br><strong>--quiet, -q</strong> 
					Reduce level of detail in output.<br><br>
					{<strong>cleanup,nav,local,run,update</strong>}<br><strong>cleanup</strong> 
					Remove stale price data.<br><strong>nav</strong> 
					Calculate a route between two systems.<br><strong>local</strong> 
					Calculate local systems.<br><strong>run</strong> 
					Calculate best trade run.<br>
					<br><br><strong>For help on a specific 
					command, use the command followed by -h.<br></strong><br>usage: run 
						--credits CR [-h] [--ship shiptype] [--capacity N]<br>
						[--from STATION] [--to STATION] [--via 
						PLACE[,PLACE,...]]<br>[--avoid AVOID] [--hops N] 
						[--jumps-per N] [--ly-per N.NN]<br>
						[--limit N] 
						[--unique] [--margin N.NN] [--insurance CR]<br>[--routes 
						N] [--checklist]<br><br>Required Arguments:<br>
						--credits CR Starting credits.<br><br>Optional Switches:<br>
						<strong>-h, --help</strong> Show this help message and exit.<br>
			<strong>--ship 
						shiptype</strong> Set capacity and ly-per from ship type.<br>
						<strong>--capacity N </strong>Maximum capacity of cargo hold.<br>
			<strong>--from 
						STATION </strong>Starting system/station.<br><strong>--to STATION</strong> Final 
						system/station.<br><strong>--via PLACE[,PLACE,...]</strong><br>Require 
						specified systems/stations to be en-route.<br><strong>--avoid 
						AVOID</strong> Exclude an item, system or station from trading.<br>
						Partial matches allowed, e.g. "dom.App" or "domap"<br>
						matches "Dom. Appliances".<br><strong>--hops N</strong> Number of hops 
						(station-to-station) to run.<br><strong>--jumps-per N 
			</strong>Maximum 
						number of jumps (system-to-system) per hop.<br><strong>--ly-per 
						N.NN</strong> Maximum light years per jump.<br><strong>--limit N 
			</strong>Maximum 
						units of any one cargo item to buy (0:<br>unlimited).<br>
						<strong>--unique</strong> Only visit each station once.<br>
			<strong>--margin N.NN 
						</strong>Reduce gains made on each hop to provide a margin of<br>
						error for market fluctuations (e.g: 0.25 reduces gains 
						by 1/4). 0&lt;: N&lt;: 0.25.<br><strong>--insurance CR</strong> Reserve at 
						least this many credits to cover insurance.<br><strong>--routes 
						N</strong> Maximum number of routes to show. DEFAULT: 1<br>
						<strong>--checklist</strong> Provide a checklist flow for the route.<br>
						</div>
	</div>
    </div>
	<br>
	<br>
</body>
</html>
<div class="auto-style7">
"Trade Dangerous Web was created using assets and imagery from Elite: Dangerous, with the permission of Frontier Developments plc, for non-commercial purposes. It is not endorsed by nor reflects the views or opinions of Frontier Developments and no employee of Frontier Developments was involved in the making of it."
</div>'));
