
<!DOCTYPE html>
<html>
	<head> 
	   <link rel="stylesheet" href="style.css" />
		<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
		<title>TDOW</title>
		<script>
			function dragResize(edge){
				overwolf.windows.getCurrentWindow(function(result){
					if (result.status=="success"){
						overwolf.windows.dragResize(result.window.id, edge);
					}
				});
			};
			
			function dragMove(){
				overwolf.windows.getCurrentWindow(function(result){
					if (result.status=="success"){
						overwolf.windows.dragMove(result.window.id);
					}
				});
			};
			
			function closeWindow(){
				overwolf.windows.getCurrentWindow(function(result){
					if (result.status=="success"){
						overwolf.windows.close(result.window.id);
					}
				});
			};
			
			function openSubWindow(){
				alert("the subwindow will only be visible inside a game");
				overwolf.windows.obtainDeclaredWindow("SubWindow", function(result){
					if (result.status == "success"){
						overwolf.windows.restore(result.window.id, function(result){
								console.log(result);
						});
					}
				});
			};
			
			function takeScreenshot(){
				overwolf.media.takeScreenshot(function(result){
					if (result.status == "success"){
						var img = document.getElementById("screenshot");
						img.src = result.url;
						img.onload = function() {
							overwolf.media.shareImage(img, "Screen Shot");
						};
					}
				});
			}
			
			function runTeamSpeak(){
				overwolf.extensions.launch("lafgmhfbkjljkgoggomibmhlpijaofafbdhpjgif");
			};
		</script>
	</head>

	<body>
		<div class="resizeGrip" id="resizeGripTopLeft" onmousedown="dragResize('TopLeft');"></div>
		<div class="resizeGrip" id="resizeGripTop" onmousedown="dragResize('Top');"></div>
		<div class="resizeGrip" id="resizeGripTopRight" onmousedown="dragResize('TopRight');"></div>
		<div class="resizeGrip" id="resizeGripRight" onmousedown="dragResize('Right');"></div>
		<div class="resizeGrip" id="resizeGripBottomRight" onmousedown="dragResize('BottomRight');"></div>
		<div class="resizeGrip" id="resizeGripBottom" onmousedown="dragResize('Bottom');"></div>
		<div class="resizeGrip" id="resizeGripBottomLeft" onmousedown="dragResize('BottomLeft');"></div>
		<div class="resizeGrip" id="resizeGripLeft" onmousedown="dragResize('Left');"></div>
		<div id="content" onmousedown="dragMove();">
<pre>			
<br />
<b>Notice</b>:  Undefined index: command_line in <b>C:\xampp\htdocs\tester1.php</b> on line <b>79</b><br />
/tradedangerous/trade.py: Error in command line: No sub-command specified.
usage: trade.py [-h] [--debug] [--detail] [--quiet] [--db DB] [--cwd CWD]
                {cleanup,nav,local,run,update} ...

Trade run calculator

Common Switches:
  -h, --help    <script>window.scrollTo(0,99999);</script>        Show this help message and exit.
  --debug, -w           Enable diagnostic output.
  --detail, -v          Increase level of detail in output.
  --quiet, -q           Reduce level of detail in output.
  --db DB               Specify location of the<script>window.scrollTo(0,99999);</script> SQLite database. Default:
                        ./data/TradeDangerous.db
  --cwd CWD, -C CWD     Change the directory relative to which TradeDangerous
                        will try to access files such as the .db, etc.

Commands:
  {cleanup,nav,local<script>window.scrollTo(0,99999);</script>,run,update}
    cleanup             Remove stale price data.
    nav                 Calculate a route between two systems.
    local               Calculate local systems.
    run                 Calculate best trade run.
    update              Update p<script>window.scrollTo(0,99999);</script>rices for a station.

For help on a specific command, use the command followed by -h.

Note: As of v3 you need to specify one of the 'sub-commands' listed above (run, nav, etc).
<script>window.scrollTo(0,99999);</script><br /><br />Trade Run Finished<br /><br /><a href="/form.php">Return to Console Screen</a></pre>
	</body>
</html>
