#README #

Welcome to Trade Dangerous OverWolf

### How do I get set up? ###

Understand this is a concept program and not a fully functional piece of software you can click one button to install. There are bugs.

In order to have this running you need to install a few things. 

First - Trade Dangerous and Python. https://bitbucket.org/kfsone/tradedangerous

Next you need to install an Apache server. I recommend XAMPP as that it is guided and easy to install. https://www.apachefriends.org/index.html

Next put everything here except TDOW in your "HTDOCS" folder.

Edit Line 16 of form.php "$command =("python /tradedangerous/trade.py ");" to point to your location of TD

TD Webs should be running. Go to localhost/form.php. If it is not running, try localhost:8080/form.php

I recommend opening the XAMPP control panel, click config next to apache and clicking the httpd.conf file and look for, 
```
#!conf

#Listen 12.34.56.78:80
#Listen 80
```
Place 

```
#!conf
Listen 8080

```

 Below that code.


Now to get it into Overwolf. Grab the Developer APP http://developers.overwolf.com/odk-2-0-introduction/ and follow the instructions for installing the "Demo app" but in place of the demo app pick the TDOW folder.

Now to get overwolf to work with Elite Dangerous, you need to go to "C:\Users\<yourusername>\AppData\Local\Overwolf" and open the "gamelist#####.xml" file.

Insert the below code below the last game on the list. 


```
#!xml

<GameInfo>
  <ID>ENTER NEXT NUMBER HERE</ID>
  <GameTitle>Elite Dangerous</GameTitle>
  <LuancherNames>
      <string>EliteDangerous32.exe</string>
  </LuancherNames>
  <GameRenderers>D3D11</GameRenderers>
  <InjectionDecision>Supported</InjectionDecision>
</GameInfo>
```
Between the <ID> tags, insert the next number. IE..game above is 105901, insert 105902.

You should be able to launch TD in game now. :)


This project is open to all to do with what they want. Have fun and happy coding!